// swift-tools-version:5.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "AppleNotificator",
    platforms: [
        .iOS("13.0"),
        .macOS("10.15")
    ],
    products: [
        .library(
            name: "AppleNotificator",
            targets: ["AppleNotificator"]),
    ],
    dependencies: [
    .package(url: "https://gitlab.com/MeerkatSync/meerkatservercore", .branch("develop")),
    .package(url: "https://github.com/LeoNavel/nio-apns.git", .branch("master")),
    ],
    targets: [
        .target(
            name: "AppleNotificator",
            dependencies: ["MeerkatServerCore", "NIOAPNS"]),
        .testTarget(
            name: "AppleNotificatorTests",
            dependencies: ["AppleNotificator"]),
    ]
)
