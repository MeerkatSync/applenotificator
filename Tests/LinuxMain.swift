import XCTest

import AppleNotificatorTests

var tests = [XCTestCaseEntry]()
tests += AppleNotificatorTests.allTests()
XCTMain(tests)
