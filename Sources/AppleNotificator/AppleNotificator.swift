//
//  AppleNotificator.swift
//  
//
//  Created by Filip Klembara on 01/03/2020.
//

import MeerkatServerCore
import Foundation
import Combine
import NIOAPNS

var cached: AppleNotificator? = nil

public final class AppleNotificator: Notificator {
    private let worker: Worker
    public let env: Environment
    public let configuration: Configuration
    private let url: URL
    init(env: Environment, worker: Worker, configuration: Configuration) {
        self.worker = worker
        self.env = env
        self.configuration = configuration
        url = env.isRelease ? URL(string: "https://api.push.apple.com/3/device/")! : URL(string: "https://api.development.push.apple.com/3/device/")!
    }

    private func _conf() -> APNSConfiguration {
        try! APNSConfiguration(keyId: configuration.kid, teamId: configuration.iss, privateKeyPath: configuration.privateKeyPath, server: env.isRelease ? .production : .development)
    }
    private var _configuration: APNSConfiguration?

    private var lastDate = Date.distantPast
    private var apnsConfig: APNSConfiguration {
        get {
            if Date().timeIntervalSince(lastDate) > 60 * 50 {
                lastDate = Date()
                _configuration = _conf()
            }
            return _configuration!
        }
    }

    func urlRequest(for token: String) -> URLRequest {
        var req = URLRequest(url: url.appendingPathComponent(token), timeoutInterval: 10)
        alertHeaders.forEach { k, v in
            req.addValue(v, forHTTPHeaderField: k)
        }
        req.httpMethod = "POST"
        let payload = APNSPayload(notificationItems: [.badge(0), .sound(""), .customPayload("meerkat-sync", NotifPayload())])
        let body = payload.jsonString!
        req.httpBody = body.data(using: .utf8)!
        return req
    }

    func backgroundUrlRequest(for token: String) -> URLRequest {
        var req = URLRequest(url: url.appendingPathComponent(token), timeoutInterval: 10)
        backgroundHeaders.forEach { (k, v) in
            req.addValue(v, forHTTPHeaderField: k)
        }
        req.httpMethod = "POST"
        let payload = APNSPayload(notificationItems: [.contentAvailable, .customPayload("meerkat-sync", NotifPayload())])
        let body = payload.jsonString!
        req.httpBody = body.data(using: .utf8)!
        return req
    }

    public func canNotify(_ device: Device) -> EventLoopFuture<Bool> {
        worker.future(device.description == "appleDevice")
    }
    
    struct ReqError: Error {
        let statusCode: Int
        let body: String?
    }

    struct NotifPayload: Encodable {
        let pullRequest = true
    }

    var cancSet = Set<AnyCancellable>()

    func getBody(d: Data, r: URLResponse) throws -> String {
        guard let resp = r as? HTTPURLResponse else {
            throw URLError(.badServerResponse)
        }
        let str = String(data: d, encoding: .utf8)
        guard resp.statusCode == 200, let s = str else {
            print("bad response \(resp.statusCode), \(str ?? "??")")
            throw URLError(.badServerResponse)
        }
        return s
    }

    public func sendNotification(to device: Device) -> EventLoopFuture<Void> {
        let pub1 = URLSession.shared.dataTaskPublisher(for: backgroundUrlRequest(for: device.id)).tryCompactMap(getBody)

        let pub = URLSession.shared.dataTaskPublisher(for: urlRequest(for: device.id)).tryCompactMap(getBody).map { ok1 in
            pub1.map { (ok1, $0) }
        }.sink(receiveCompletion: { res in
            switch res {
            case .finished:
                break
            case .failure(let err):
                print("notification error: ", err)
            }
        }, receiveValue: { _ in

        })
        pub.store(in: &cancSet)
        return worker.future()
    }
}

extension AppleNotificator {
    private var headersCore: [String: String] {
        [
            "content-type": "application/json; charset=utf-8",
            "apns-topic": configuration.apnsTopic,
            "authorization": apnsConfig.authorizationHeader,
        ]
    }


    private var alertHeaders: [String: String] {
        let headers = [
            "apns-expiration": "\(APNSExpiration.immediate.rawValue)",
            "apns-priority": "\(APNSPriority.immediate.rawValue)",
            "apns-push-type": "alert"
        ]

        return headers.merging(headersCore) { a, b in a }
    }

    private var backgroundHeaders: [String: String] {
        let headers = [
            "apns-expiration": "\(APNSExpiration.relative(3600).rawValue)",
            "apns-priority": "\(APNSPriority.background.rawValue)",
            "apns-push-type": "background"
        ]
        return headers.merging(headersCore) { a, b in a }
    }
}

extension AppleNotificator: ServiceType {
    public static func makeService(for container: Container) throws -> AppleNotificator {
        if cached == nil {
            let conf = try container.make(AppleNotificator.Configuration.self)
            cached = .init(env: container.environment, worker: container, configuration: conf)
        }
        return cached!
    }
}
