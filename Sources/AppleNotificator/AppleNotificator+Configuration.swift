//
//  AppleNotificator+Configuration.swift
//  
//
//  Created by Filip Klembara on 09/03/2020.
//

import Foundation
import Service

extension AppleNotificator {
    public struct Configuration {
        public let kid: String
        public let iss: String
        public let apnsTopic: String
        public let privateKeyPath: String

        public init(kid: String, iss: String, apnsTopic: String, privateKeyPath: String) {
            self.kid = kid
            self.iss = iss
            self.apnsTopic = apnsTopic
            self.privateKeyPath = privateKeyPath
        }
    }
}

extension AppleNotificator.Configuration: Service { }
